<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'home';
$template['template_dir'] = 'template';
/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| iservice Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/
$template['home']['template'] = 'template/home/index';
$template['home']['template_core'] = 'index';
$template['home']['regions'] = array('detail','header','footer');
$template['home']['regions_map'] = '';
$template['home']['parser'] = 'parser';
$template['home']['parser_method'] = 'parse';
$template['home']['parser_template'] = FALSE;
$template['home']['css'] = array();
$template['home']['js'] = array();
$template['home']['doctype'] = 'html5';
$template['home']['use_favicon'] = FALSE;
$template['home']['favicon_location'] = '';
$template['home']['meta_content'] = 'UTF-8';
$template['home']['meta_language'] = 'en-US';
$template['home']['meta_author'] = '';
$template['home']['meta_description'] = '';
$template['home']['meta_keywords'] = '';
$template['home']['body_id'] = '';
$template['home']['title'] = '';

$template['engines']['template'] = 'template/engines/index';
$template['engines']['template_core'] = 'index';
$template['engines']['regions'] = array('detail','header','footer');
$template['engines']['regions_map'] = '';
$template['engines']['parser'] = 'parser';
$template['engines']['parser_method'] = 'parse';
$template['engines']['parser_template'] = FALSE;
$template['engines']['css'] = array();
$template['engines']['js'] = array();
$template['engines']['doctype'] = 'html5';
$template['engines']['use_favicon'] = FALSE;
$template['engines']['favicon_location'] = '';
$template['engines']['meta_content'] = 'UTF-8';
$template['engines']['meta_language'] = 'en-US';
$template['engines']['meta_author'] = '';
$template['engines']['meta_description'] = '';
$template['engines']['meta_keywords'] = '';
$template['engines']['body_id'] = '';
$template['engines']['title'] = '';

/* End of file template.php */
/* Location: ./system/application/config/template.php */