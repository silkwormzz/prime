<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->templateDir = 'layout/main';
		$this->load->model('payment_model', 'm_payment');
		$this->load->model('email_model', 'm_email');
		$this->template->set_template('home');
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index()
	{
		$error = '';

		$data['error'] = $error;
		// $this->load->view('template/home/header');
		// $this->load->view('layout/main/home');

		// // if($this->session->userdata('logged_in')){
		$this->template->write_view('header', 'template/home/header');
		$this->template->write_view('detail', $this->templateDir . '/home', $data, TRUE);
		$this->template->render();
		// // }else{
		// // 	$this->template->write_view('header', 'template/home/header-top');
		// // 	$this->template->write_view('detail', $this->templateDir.'/home', $data, TRUE);
		// // 	$this->template->render();
		// // }
	}

	public function invoice()
	{
		$input = $this->input->post();
		$hospital_no = $input['hospital_no'];
		$invoice_no = $input['invoice_no'];
		$bank_payment = $input['bank_payment'];
		if (!empty($bank_payment)) {
			$percentage_fee = $this->m_payment->getPrecentageFee($bank_payment);
		}

		// var_dump($fee);
		$insert['invoice_no'] = $invoice_no;
		$insert['hospital_number'] = $hospital_no;
		$insert['payment_channel'] = $bank_payment;
		$insert['payment_status'] = '1';
		$insert['created_date'] = date('Y-m-d H:s:i');
		$insert['state'] = '1';

		// $payment = $this->m_payment->setInvoice($data);

		// var_dump($input['hospital_no']);
		if (!empty($hospital_no) && !empty($invoice_no)) {
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_PORT => "8295",
				CURLOPT_URL => "http://110.49.49.226:8295/prime/v2/inv",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				// CURLOPT_POSTFIELDS => "{\"INV\" : \"INV19100564\",\"HN\" : \"190343-F\"}",
				CURLOPT_POSTFIELDS => "{\"INV\" : \"" . $invoice_no . "\",\"HN\" : \"" . $hospital_no . "\"}",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic cHJpbWVhcGk6NmIyQ2QlSiheYURs",
					"Content-Type: application/json",
				),
			));

			$response = curl_exec($curl);

			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				$data['response'] = $err;
				// echo "cURL Error #:" . $err;
			} else {
				// echo $response;\
				// echo '<pre>';
				// print_r($percentage_fee);
				$json_response = json_decode($response);

				if ($json_response->status == 'Success') {
					// echo '<pre>';
					// print_r($json_response);
					// exit;
					$data['response'] = $json_response;
					$amount = $json_response->cus_info->PriceTotal;
					$fee_total = ($amount * $percentage_fee['0']->percentage_fee) / 100;
					$total = $amount + $fee_total;
					$discount = $json_response->cus_info->DiscountTotal;
					$grand_total = $total - $discount;
					$data['amount'] = $amount;
					$data['fee'] = $fee_total;
					$data['total'] = $total;
					$data['discount'] = $discount;
					$data['grand_total'] = $grand_total;
					$data['price_th'] = $this->price_th_convert(number_format($grand_total, 2));
					$data['percentage_fee'] = $percentage_fee['0']->percentage_fee;
					$data['payment_name'] = $percentage_fee['0']->payment_name;
					$insert['amount'] = $amount;
					$insert['discount'] = $discount;
					$insert['fee'] = $fee_total;
					$insert['total'] = $total;
					$data['payment'] = $this->m_payment->setInvoice($insert);
				} else {
					$data['response'] = $json_response;
					$amount = $json_response->cus_info->PriceTotal;
					$fee_total = ($amount * $percentage_fee['0']->percentage_fee) / 100;
					$total = $amount - $fee_total;
					$discount = $json_response->cus_info->DiscountTotal;
					$grand_total = $total - $discount;
					$data['response'] = $json_response;
					$data['amount'] = 0;
					$data['fee'] = 0;
					$data['total'] = 0;
					$data['discount'] = 0;
					$data['grand_total'] = 0;
					$data['price_th'] = $this->price_th_convert(number_format(0, 2));
					$data['percentage_fee'] = 0;
					$data['payment_name'] = $percentage_fee['0']->payment_name;
					$insert['amount'] = 0;
					$insert['fee'] = 0;
					$insert['total'] = 0;
					$insert['discount'] = 0;
					$insert['payment_status_note'] = 'Not Match';
				}
			}
		}

		$this->template->write_view('header', 'template/home/header');
		$this->template->write_view('detail', $this->templateDir . '/invoice', $data, TRUE);
		$this->template->render();
	}

	public function success()
	{
		// echo 'success';
		// exit();

		$response = file_get_contents('php://input');
		$payment_status = $_REQUEST["payment_status"];
		//000 = Payment Successful
		//001 = Payment Pending
		//002 = Payment Rejected
		//003 = Payment was canceled by user
		//999 = Payment Failed

		// var_dump($payment_status);
		// exit();

		// if (!empty($payment_status)) { 

		if (!empty($payment_status) && $payment_status == '000') { 
			// $invoice = $this->m_payment->insertInvoice($payment_status);
			$invoice_id = $this->input->get('invoice_id');
			// // var_dump($invoice_id);
			// // echo '<br/>';
			$invoice = $this->m_payment->getInvoiceByID($invoice_id);

			// if(!empty($payment_status)){
			$update = $this->m_payment->UpdateInvoice($invoice_id, $invoice, $payment_status);
			$data['payment'] = $update;
			if(!empty($update)){
				$this->sendEmail($invoice, $payment_status);
			}
			
			// }

			$this->template->write_view('header', 'template/home/header');
			$this->template->write_view('detail', $this->templateDir . '/success', '', TRUE);
			$this->template->render();
		} else {
			$this->template->write_view('header', 'template/home/header');
			$this->template->write_view('detail', $this->templateDir . '/fail', '', TRUE);
			$this->template->render();
		}
	}

	public function confirm()
	{
		// echo 'success';
		// exit();

		$response = file_get_contents('php://input');
		$payment_status = $_REQUEST["payment_status"];
		//000 = Payment Successful
		//001 = Payment Pending
		//002 = Payment Rejected
		//003 = Payment was canceled by user
		//999 = Payment Failed

		// var_dump($payment_status);
		// exit();

		if (!empty($payment_status) && $payment_status == '000') {
			$invoice = $this->m_payment->insertInvoice($payment_status);
			// $invoice_id = $this->input->get('invoice_id');
			// // var_dump($invoice_id);
			// // echo '<br/>';
			// $invoice = $this->m_payment->getInvoiceByID($invoice_id);

			// if(!empty($payment_status)){
			// 	$data['payment'] = $this->m_payment->UpdateInvoice($invoice_id, $invoice, $payment_status);
			// }

			// $this->template->write_view('header', 'template/home/header');			
			// $this->template->write_view('detail', $this->templateDir.'/success', '', TRUE);					
			// $this->template->render();
			// }else{
			// 	$this->template->write_view('header', 'template/home/header');			
			// 	$this->template->write_view('detail', $this->templateDir.'/fail', '', TRUE);					
			// 	$this->template->render();
		}
	}


	public function price_th_convert($number = null)
	{
		$txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
		$txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
		$number = str_replace(",", "", $number);
		$number = str_replace(" ", "", $number);
		$number = str_replace("บาท", "", $number);
		$number = explode(".", $number);

		if (sizeof($number) > 2) {
			return 'ทศนิยมหลายตัวนะจ๊ะ';
			exit;
		}
		$strlen = strlen($number[0]);


		$convert = '';
		for ($i = 0; $i < $strlen; $i++) {
			$n = substr($number[0], $i, 1);
			if ($n != 0) {
				if ($i == ($strlen - 1) and $n == 1) {
					$convert .= 'เอ็ด';
				} elseif ($i == ($strlen - 2) and $n == 2) {
					$convert .= 'ยี่';
				} elseif ($i == ($strlen - 2) and $n == 1) {
					$convert .= '';
				} else {
					$convert .= $txtnum1[$n];
				}
				$convert .= $txtnum2[$strlen - $i - 1];
			}
		}

		$convert .= 'บาท';
		if (
			$number[1] == '0' or $number[1] == '00' or
			$number[1] == ''
		) {
			$convert .= 'ถ้วน';
		} else {
			$strlen = strlen($number[1]);
			for ($i = 0; $i < $strlen; $i++) {
				$n = substr($number[1], $i, 1);
				if ($n != 0) {
					if ($i == ($strlen - 1) and $n == 1) {
						$convert
							.= 'เอ็ด';
					} elseif (
						$i == ($strlen - 2) and
						$n == 2
					) {
						$convert .= 'ยี่';
					} elseif (
						$i == ($strlen - 2) and
						$n == 1
					) {
						$convert .= '';
					} else {
						$convert .= $txtnum1[$n];
					}
					$convert .= $txtnum2[$strlen - $i - 1];
				}
			}
			$convert .= 'สตางค์';
		}
		return $convert;
		// } 
		## วิธีใช้งาน
		// $x = '9123568543241.25'; 
		// echo  $x  . "=>" .convert($x); 

	}

	public function sendEmail($invoice = null, $payment_status = null)
	{
		$set_mail = $this->m_email->getEmail();
		// var_dump($invoice['0']->);
		// exit();
		if(!empty($set_mail)){
			if($payment_status == '000'){
				$status = 'Complete';
			}else{
				$status = 'Incomplete';
			}
			// $to = "reballsion@gmail.com";
			// $from = "reballsion@gmail.com";
			$to = $set_mail['0']->to;
			$from = $set_mail['0']->from;
			$subject = "ผลการชำระเงินของInvoice No.: ".$invoice['0']->invoice_no;
			$message = "เรียนทุกท่าน \r\n";		
			$message .= "ผลการชำระเงินของInvoice No.: ".$invoice['0']->invoice_no ."\r\n";
			$message .= "จำนวนเงิน: ". $invoice['0']->total ."\r\n";
			$message .= "วันที่ชำระเงิน: ".$invoice['0']->payment_date ."\r\n";
			$message .= "สถานะ: ".$status;
			$headers = "From: ". $set_mail['0']->from_name."\r\n";
			$headers .= "Cc: ". $set_mail['0']->cc."\r\n";
	 
			mail($to, $subject, $message, $headers, "-f " . $from);
		}
		
		//check if the email address is invalid $secure_check
		// $secure_check = mail($to_email,$subject,$message,$headers);
		// if ($secure_check == false) {
		//     echo "Invalid input";
		// } else { //send email 
		//     // mail($to_email, $subject, $message, $headers);
		//     echo "This email is sent using PHP Mail";
		// }
	}
}
