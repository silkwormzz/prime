<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// echo error_reporting(-1);
session_start();

class User extends CI_Controller
{	
	public $templateDir;
	public function __construct()
	{
		parent::__construct();

		$this->templateDir = 'layout/engines';
		$this->load->model('employee_model', 'm_employee');
		$this->load->helper('form', 'url');
		$this->load->library('mypagination');
		$this->template->set_template('engines');

	}

	public function index()
	{
		

		// if($this->session->userdata('logged_in'))
		// {
		// 	$session_data = $this->session->userdata('logged_in');

		// 	$per_page = 10;

			$total_Employee = $this->m_employee->getAllEmployee();

			$config['total_rows'] = count($total_Employee);
			$config['per_page'] = $per_page;
			$config['base_url'] = base_url('engines/employee/index');
			$config["uri_segment"] = 4;
			$config['full_tag_open'] = '<li>';
			$config['full_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li><a class="active">';
			$config['cur_tag_close'] = '</a></li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

			$data['page'] = $page;
			$data['employee'] = $this->m_employee->getEmployeePage($page); 

			$this->mypagination->initialize($config); 

			$data['pagination'] = $this->mypagination->create_links();

			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/employee/home', $data , TRUE);
			$this->template->write_view('footer', 'template/engines/footer');
			$this->template->render();
		// }
		// else
		// {
		//  redirect(base_url('engines'));
		// }
	}

	public function add()
	{
		// $this->template->set_template('engines');

		// if($this->session->userdata('logged_in'))
		// {
		// 	$session_data = $this->session->userdata('logged_in');
		// }
		// else
		// {
		//  redirect(base_url('engines'));
		// }

		$this->template->write_view('header', 'template/engines/header');
		$this->template->write_view('detail', $this->templateDir.'/employee/employee');
		$this->template->write_view('footer', 'template/engines/footer');
		$this->template->render();
	}

	public function validation()
	{

		var_dump($this->input->post());
		echo '<br>';
		var_dump($_FILES);
		echo '<br>';
		// // echo count($_FILES)-1;
		exit();
		// if($this->session->userdata('logged_in'))
		// {
			// $session_data = $this->session->userdata('logged_in');
			
			$firstname = $this->input->post('firstname');
			$lastname = $this->input->post('lastname');
			$employee_id = $this->input->post('employee_id');
			// $username = $this->input->post('username');
			// $password = $this->input->post('password');
			// $email = $this->input->post('employee_email');
			$state_employee = $this->input->post('status');

			$folder_name = $employee_id;
			$assets_path = 'assets/home/img/uploads/'.$folder_name;
			// $path = realpath($assets_path);

			if(!is_dir($assets_path)){
				mkdir($assets_path, 0777, TRUE);
				chmod($assets_path, 0777);		                
            }

			// var_dump($this->db->list_tables());
			// exit();

// 			if(!empty($username) && !empty($password) && !empty($email)){
// 				// $user['id'] = "";
// 	        	$user['username'] = $username;
// 	        	$user['password'] = $password;
// 	        	$user['email'] = $email;
// 	        	$user['state'] = $state;
// 	        	$user['created_date'] = date('Y-m-d H:s:i');
// 	        	// $user['created_by'] = $session_data['name'];
// 	        	$user['modifiled_date'] = date('Y-m-d H:s:i');
// // 	        	$this->load->database();

// // print_r($this->db);
//  				$getUser = $this->m_employee->getUser($email);

//  				// var_dump($getUser);
//  				// echo '22';
//  				// echo '<br>';
//  				// exit();
//  				if(empty($getUser)){
//  					$setUser = $this->m_employee->setUser($user);			
// 					$user_id = $setUser;
//  				}else{
//  					$user_id = $getUser['0']->id;
//  				}

//  				// var_dump($user_id);
//  				// exit();
// 			}

			if(!empty($firstname) && !empty($lastname) && !empty($employee_id)){
				// $data['id'] = "";
				// $data['user_id'] = $user_id;
	        	$data['first_name'] = $firstname;
	        	$data['last_name'] = $lastname;
	        	$data['employee_id'] = $employee_id;
	        	$data['state'] = $state_employee;
	        	$data['created_date'] = date('Y-m-d H:s:i');
	        	// $data['created_by'] = $session_data['name'];
	        	// $this->add($data);
	        	$getEmployeeID = $this->m_employee->getEmployeeID($employee_id);

	        	// var_dump($employee_id);
	        	// echo '<br>';
	        	// var_dump($getEmployeeID);
	        	// echo '<br>';
	        	// var_dump($_FILES['image_employee']);
	        	// exit();
	        	
	        	if(empty($getEmployeeID)){

	        		// var_dump($data);
	        		// exit();
					$setEmployee = $this->m_employee->setEmployee($data);			
					$profile_id = $setEmployee;

					// var_dump($profile_id);
					// echo '<br>';

					// // var_dump($_FILES['image_employee']);
					// exit();

		        	$image_employee = $_FILES['image_employee'];
					$ext = explode( '.', $image_employee['name']);
					$file_ext = $ext['1'];

					// var_dump($file_ext);
					// exit();

					// $folder_name = $employee_id;
					// $assets_path = 'assets/home/img/uploads/'.$folder_name;
					// var_dump($assets_path);
					$path = realpath($assets_path);

					// if(!is_dir($assets_path)){
					// 	echo 'aa';
					// // // 	// echo '<br>';
					// 	mkdir($assets_path, 0777, TRUE);		                
		   //          }else{
		   //          	echo 'ddd';
		   //          }

					// var_dump($profile_id);
					// exit();

					$employee_image['overwrite'] = TRUE;
					$employee_image['allowed_types'] = 'jpg|jpeg|png';
					$employee_image['max_size'] = 2048;
					$employee_image['upload_path'] = $path;
					$employee_image['file_name'] = $employee_id;
					$this->load->library('upload', $employee_image);

					$data['image'] = $employee_id.'.'.$file_ext;

					if (!$this->upload->do_upload('image_employee')){
						$error = $this->upload->display_errors('<p>', '</p>');
						echo "<script>alert('".$error."');</script>";
			        }else{
			        	$updateEmployee = $this->m_employee->updateEmployee($profile_id, $data);			
			        }
	        	}else{
	        		$profile_id = $getEmployeeID['0']->id;
	        		if(empty($getEmployeeID['0']->image)){
	        			if(!empty($_FILES['image_employee'])){
	        				$image_employee = $_FILES['image_employee'];
							$ext = explode( '.', $image_employee['name']);
							$file_ext = $ext['1'];
							$path = realpath($assets_path);
							$employee_image['overwrite'] = TRUE;
							$employee_image['allowed_types'] = 'jpg|jpeg|png';
							$employee_image['max_size'] = 2048;
							$employee_image['upload_path'] = $path;
							$employee_image['file_name'] = $employee_id;
							$this->load->library('upload', $employee_image);

							$data['image'] = $employee_id.'.'.$file_ext;

							if (!$this->upload->do_upload('image_employee')){
								$error = $this->upload->display_errors('<p>', '</p>');
								echo "<script>alert('".$error."');</script>";
					        }else{
					        	$updateEmployee = $this->m_employee->updateEmployee($profile_id, $data);			
					        }
	        			}
	        		}
	        	}

	        	// exit();
	        	
	    //     	var_dump($profile_id);
					// echo '<br>';
				
	        }

	    //     var_dump($profile_id);
					// echo '<br>';

	        // var_dump($_FILES);
	        // echo '<br>';
	        // echo count($_FILES)-2;
	        // exit();

			for ($i=0; $i<count($_FILES)-2; $i++) {
				$lens_name = 'lens_name_'.$i;
				$lens_img = 'image_'.$i;
				$state_lens = 'state_'.$i;
				$image_hidden = 'image_hidden_'.$i;

				// echo '<pre>';
				// var_dump($_FILES);
				// // var_dump($_FILES[$lens_img]['name']);

				// echo '</pre>';
				// echo '<br>';
				// exit();
				$sub_folder_name = $employee_id.'_'.$i;
			 	$sub_path_name = $assets_path.'/'.$sub_folder_name;
				// $sub_path = realpath($sub_path_name);
				// $data_lens['lens_name'] = $lens_names[$i];
				// $sub_path = $path .'/'. $sub_folder_name;
				
				
				if(!is_dir($sub_path_name)){
	                mkdir($sub_path_name, 0777, TRUE);
	                chmod($sub_path_name, 0777);

	            }
	            

				if(!empty($this->input->post($lens_name))){
					// var_dump($this->input->post());
					// exit();
					$lens_names[$i] = $this->input->post($lens_name);
					$lens_states[$i] = $this->input->post($state_lens);
					$lens_img_hidden[$i] = $this->input->post($image_hidden);

					if(!empty($this->input->post($image_hidden))){
						$lens_img_hidden[$i] = $this->input->post($image_hidden);
					}else{
						$lens_img_hidden[$i] = '';
					}
					if(!empty($this->input->post($state_lens))){
						$lens_states[$i] = $this->input->post($state_lens);
					}else{
						$lens_states[$i] = '1';
					}
					for ($j=0; $j<count($_FILES[$lens_img]['name']); $j++) { 
					// 	// $lens_image_names[$i][$j] = $_FILES[$lens_img]['name'][$j];
						$ext = explode( '.', $_FILES[$lens_img]['name'][$j]);
						$file_ext = $ext['1'];
						
						$lens_image_names[$i][$j] = $employee_id."_".$i."_".$j.".".$file_ext;
						
					// 	// var_dump($j);
					// 	// echo '<br>';
					// 	// // var_dump($lens_img);
					// 	// // echo '<br>';
					// 	// var_dump($_FILES[$lens_img]['tmp_name'][$j]);
					// 	// echo '<br>';
					}
					
				}
				
				// var_dump($lens_name_.$i);
				// echo 'lens_name_'.$i;
				// echo '<br>';
			}

// exit();
			// var_dump($profile_id);
			// echo '<br>';

			if(!empty($lens_names)){
				// $data_lens['id'] = "";
				// $data_lens['user_id'] = $user_id;
				$data_lens['profile_id'] = $profile_id;
	        	$data_lens['state'] = $state;
	        	$data_lens['created_date'] = date('Y-m-d H:s:i');
	        	// $data_lens['created_by'] = $session_data['name'];
	        	// $this->add($data_lens);
	    //     	$getLens = $this->m_employee->getLens($profile_id);
	        	// if(!empty($getLens)){
					// $deleteLens = $this->m_employee->deleteLens($getLens['0']->profile_id, $data_lens);
					// for ($a=0; $a<count($getLens) ; $a++) { 
					// 	$add['id'] = $getLens[$a]->id;
					// 	$add['user_id'] = $getLens[$a]->user_id;
					// 	$add['profile_id'] = $getLens[$a]->profile_id;
					// 	$add['len_name'] = $getLens[$a]->len_name;
					// 	$add['len_img1'] = $getLens[$a]->len_img1;
					// 	$add['len_img2'] = $getLens[$a]->len_img2;
					// 	$add['len_img3'] = $getLens[$a]->len_img3;
					// 	$add['len_img4'] = $getLens[$a]->len_img4;
					// 	$add['len_img5'] = $getLens[$a]->len_img5;
					// 	$add['len_img2'] = $getLens[$a]->len_img6;
					// 	$add['created_date'] = $getLens[$a]->created_date;
					// 	$add['created_by'] = $getLens[$a]->created_by;
					// }
					// // var_dump($getLens);
					// // exit();
					// // $setLens = $this->m_employee->setLens($getLens);
	    //     	}else{
	    //     		$setLens = $this->m_employee->setLens($data_lens);	
	    //     		$employee_lens_id = $setLens;
	        	// }
	        	$getLens = $this->m_employee->getLens($profile_id);
				// var_dump($getLens);
				// exit();
	        	if(!empty($getLens)){
	        		 
					$deleteLens = $this->m_employee->deleteLens($profile_id, $data_lens);

					var_dump(count($getLens));
					// // exit();
					for ($a=0; $a<count($getLens); $a++) {
					 	$len_img_name_get[$a] = $getLens[$a]->len_img;

					// 	$add['id'] = $getLens[$a]->id;
					// // 	// $add['user_id'] = $getLens[$a]->user_id;
						// $add['profile_id'] = $getLens[$a]->profile_id;
						// $add['len_name'] = $getLens[$a]->len_name;
						// $add['len_no'] = $getLens[$a]->len_no;
						// $add['len_img'] = $getLens[$a]->len_img;
						// $add['state'] = $getLens[$a]->state;
					// // 	// $add['len_img2'] = $getLens[$a]->len_img2;
					// // 	// $add['len_img3'] = $getLens[$a]->len_img3;
					// // 	// $add['len_img4'] = $getLens[$a]->len_img4;
					// // 	// $add['len_img5'] = $getLens[$a]->len_img5;
					// // 	// $add['len_img2'] = $getLens[$a]->len_img6;
						// $add['created_date'] = $getLens[$a]->created_date;
					// // 	$add['created_by'] = $getLens[$a]->created_by;
					// // 	echo '<br>';
					// // 	echo '<br>';
					// // 	echo '<br>';
					// // 	var_dump($add);
					// // 	echo '<br>';
					// // 	echo '<br>';
					// // 	echo '<br>';
					// // 	// exit();
						// $setLens = $this->m_employee->setLens($add);							
					// // }else{
					// // 	$len_img_name_get = '';
					}
					// var_dump($len_img_name_get);
					// // exit();
					// // $setLens = $this->m_employee->setLens($getLens);
	        	}else{
					$len_img_name_get = '';
				}

		   //      var_dump($lens_names);
		   //      echo '<br>';
		   //      var_dump($profile_id);
		   //      // exit();	
	        	$lens_no_get = count($getLens);

				for ($k=0; $k<count($lens_names); $k++) {
// 					// $len_no = $lens_no_get + 1
// 					// $data_lens['user_id'] = $user_id;
// 					$data_lens_new['profile_id'] = $profile_id;
// 		        	$data_lens_new['state'] = '1';
// 		        	$data_lens_new['created_date'] = date('Y-m-d H:s:i');		        	
// 					$data_lens_new['len_name'] = $lens_names[$k];
// 					$data_lens_new['len_no'] = $lens_no_get + $k;
// 					// $sub_path = realpath($sub_path_name);
// 					// $getLens = $this->m_employee->getLens($profile_id);
// 					// var_dump($getLens);
// 		   //      	if(!empty($getLens)){
// 					// 	$deleteLens = $this->m_employee->deleteLens($getLens['0']->profile_id, $data_lens);
// 					// 	for ($a=0; $a<count($getLens) ; $a++) { 
// 					// 		$add['id'] = $getLens[$a]->id;
// 					// 		$add['user_id'] = $getLens[$a]->user_id;
// 					// 		$add['profile_id'] = $getLens[$a]->profile_id;
// 					// 		$add['len_name'] = $getLens[$a]->len_name;
// 					// 		$add['len_img1'] = $getLens[$a]->len_img1;
// 					// 		$add['len_img2'] = $getLens[$a]->len_img2;
// 					// 		$add['len_img3'] = $getLens[$a]->len_img3;
// 					// 		$add['len_img4'] = $getLens[$a]->len_img4;
// 					// 		$add['len_img5'] = $getLens[$a]->len_img5;
// 					// 		$add['len_img2'] = $getLens[$a]->len_img6;
// 					// 		$add['created_date'] = $getLens[$a]->created_date;
// 					// 		$add['created_by'] = $getLens[$a]->created_by;
// 					// 		$setLens = $this->m_employee->setLens($add);							
// 					// 	}
// 					// 	// var_dump($getLens);
// 					// 	// exit();
// 					// 	// $setLens = $this->m_employee->setLens($getLens);
// 		   //      	}else{
// 		        		$setLens_new = $this->m_employee->setLens($data_lens_new);	
// 		        		$employee_lens_id = $setLens_new;
// 		        	// }
// 				 // 	$sub_folder_name = $employee_id.'_'.$i;
// 				 // 	$sub_path_name = $assets_path.'/'.$sub_folder_name;
// 					// $sub_path = realpath($sub_path_name);
// 					// $data_lens['len_name'] = $lens_names[$k];
// 					// // $sub_path = $path .'/'. $sub_folder_name;
					
					
// 					// if(!is_dir($sub_path_name)){
// 		   //              mkdir($sub_path_name, 0777, TRUE);
// 		   //              chmod($sub_path_name, 0777);

// 		   //          }
// // exit();
		        		var_dump($employee_id);
		        		echo '<br>';
		        	$lens_img = 'image_'.$k;

		        	var_dump($lens_img);
					echo '<br>';
		        		
		        		// exit();
					var_dump($_FILES[$lens_img]['tmp_name']);
					echo '<br>';
// echo '<br>';
		            for ($l=0; $l<count($_FILES[$lens_img]['tmp_name']); $l++) {
		            	$ext = explode( '.', $_FILES[$lens_img]['name'][$l]);
						$file_ext = $ext['1'];
		            	$rows = $l + 1;
		            	$len_no_k = $k + 1; 
		            	// var_dump($row);
		            	// $lensimg = 'len_img'. $row;
		            	// $data_lens[$lensimg] = $lens_image_names[$k][$l];
		            	$row = $lens_no_get + $k;
		            	// echo '<br>';
		            	// echo '<br>';
		            	// var_dump($row);
		            	// echo '<br>';
		            	// echo '<br>';
		            	$file_name = $employee_id . '-' . $l . '-' . $len_no_k;
		            	// echo $k;
		            	// echo 'k';
		            	// echo '<br>';
		            	// echo $l;
		            	// echo 'l';
		            	// echo '<br>';

		            	var_dump($file_name);
		            	echo '<br>';
		            	$data_lens_new['profile_id'] = $profile_id;
			        	$data_lens_new['state'] = $lens_states[$k];
			        	$data_lens_new['created_date'] = date('Y-m-d H:s:i');		        	
						$data_lens_new['len_name'] = $lens_names[$k];
						// $data_lens_new['len_img'] = $lens_img_hidden[$k];
						// $data_lens_new['len_no'] = $lens_no_get + $k;
						$data_lens_new['len_no'] = $len_no_k;
						$setLens_new = $this->m_employee->setLens($data_lens_new);	
		        		$employee_lens_id = $setLens_new;

						// echo $l;
		            	echo $employee_lens_id;
		            	echo '<br>';

		            	$sub_folder_name = $employee_id . '-' . $len_no_k;
					 	$sub_path_name = $assets_path . '/' . $sub_folder_name;
						// $sub_path = realpath($sub_path_name);
						// $data_lens['lens_name'] = $lens_names[$i];
						// $sub_path = $path .'/'. $sub_folder_name;
						
						echo $sub_path_name;
						echo '<br>';
						if(!is_dir($sub_path_name)){
			                mkdir($sub_path_name, 0777, TRUE);
			                chmod($sub_path_name, 0777);
			                echo '<successfully dir>';
			                echo '<br>';
			            }
			            var_dump($rows);
			            echo '<br>';
			            // $lens_img = 'image_'.$rows;
			            var_dump($file_ext);
			            echo '<br>';

		            	if($file_ext  == 'jpg' || $file_ext  == 'png' || $file_ext  == 'jpeg'){
		            		$src = $_FILES[$lens_img]['tmp_name'][$l];
		            		
	                        $dest = $sub_path_name.'/'.basename($file_name.'.'.$file_ext);

	                        var_dump($src);
	                        echo '<br>';
	                        var_dump($dest);
	                        echo '<br>';
	                        // var_dump($sub_path_name.'/'.basename($_FILES[$lens_img]['name'][$l]));
	                        // echo '<br>';
	                        
	                        // var_dump(move_uploaded_file($src, $sub_path_name));
	                        // echo '<br>';
	                        if(move_uploaded_file($src, $dest)){
	                            $image = $file_name.'.'.$file_ext;
	                            echo 'successfully file uploaded';
							    echo '<br>';
	                        }else{
	                            $image = '';
	                            echo 'something went wrong'; 
							    echo '<br>';
	                        } 
	          //               $isUploaded = move_uploaded_file($src, $dest);
							    //  if($isUploaded){
							    //    echo 'successfully file uploaded';
							    //    echo '<br>';
							    // } else{
							    //    echo 'something went wrong'; 
							    //    echo '<br>';
							   
							   	// }

	                        $data_img_lens['len_img'] = $image;
	       //                  if(!empty($_FILES[$lens_img])){
		      //   				$image_lens = $_FILES[$lens_img];
								// $ext = explode( '.', $image_lens['name']);
								// $file_ext = $ext['1'];
								// $path = realpath($assets_path);
								// $employee_image['overwrite'] = TRUE;
								// $employee_image['allowed_types'] = 'jpg|jpeg|png';
								// $employee_image['max_size'] = 2048;
								// $employee_image['upload_path'] = $path;
								// $employee_image['file_name'] = $employee_id;
								// $this->load->library('upload', $employee_image);

								// $data['image'] = $employee_id.'.'.$file_ext;

								// if (!$this->upload->do_upload('image_employee')){
								// 	$error = $this->upload->display_errors('<p>', '</p>');
								// 	echo "<script>alert('".$error."');</script>";
						  //       }else{
						  //       	$updateEmployee = $this->m_employee->updateEmployee($profile_id, $data);			
						  //       }
		      //   			}

							// var_dump($image);
	      //               	echo '<br>';
		                    // if ($employee_lens_id != "") {
								// $data_lens['id'] = $employee_lens_id;
								// var_dump($data_lens);
								$updateEmployee = $this->m_employee->updateLens($employee_lens_id, $data_img_lens);
							// }	
	                    }
           
					}
				}
			}
			// echo '<script>alert("Upload Success")</script>';
			// echo '<script>window.location.href = "/engines/employee";</script>';
		//}
	}

	public function edit()
	{
 		$profile_id = $this->input->get('id', TRUE);
 		$data['profile'] = $this->m_employee->getProfileId($profile_id);

 		$this->template->write_view('header', 'template/engines/header');
		$this->template->write_view('detail', $this->templateDir.'/employee/employee', $data , TRUE);
		$this->template->write_view('footer', 'template/engines/footer');
		$this->template->render();
 		
	}
}