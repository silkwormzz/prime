<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 // echo error_reporting(-1);
session_start();

class Report extends CI_Controller
{	
	public $templateDir;
	public function __construct()
	{
		parent::__construct();
		$this->templateDir = 'layout/engines';
		$this->load->model('payment_model', 'm_payment');
		$this->load->library('session');
		// $this->load->helper('form', 'url');
		$this->template->set_template('engines');
	}

	public function index()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		if(!$this->session->userdata('logged_in')){
			redirect(base_url('engines/payment', 'refresh'));
		}else{
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/report/report', $data, TRUE);
			$this->template->render();
		}	
	}

	public function reports()
	{
		// // echo 'eeeeee';
		// // exit();

		// // $data['error'] = $error;
		if(!$this->session->userdata('logged_in')){
			redirect(base_url('engines/payment', 'refresh'));
		}else{
			$input = $this->input->post();
			$start_date = $input['start_date'];
			$end_date = $input['end_date'];
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$report = $input['report'];
			if(!empty($report)){
				switch ($report) {
					case '1':
						$data['data'] = $this->m_payment->getReportPayment($start_date, $end_date);
						// var_dump($data);
						$page = 'report_payment';
						break;

					case '2':
						$data['data'] = $this->m_payment->getReportPaymentComplete($start_date, $end_date);
						// var_dump($data);
						$page = 'report_payment_complate';
						break;

					case '3':
						$data['data'] = $this->m_payment->getReportPaymentUnComplete($start_date, $end_date);
						// var_dump($data);
						$page = 'report_payment_uncomplate';
						break;
				}
			}elseif(!empty($start_date) || !empty($end_date)){
				$this->template->write_view('header', 'template/engines/header');
				$data['error'] = 'กรุณาเลือกวันที่เริ่่มต้นและวันที่สิ้นสุด';
				$page = 'report';
			}else{
				$this->template->write_view('header', 'template/engines/header');
				$data['error'] = 'กรุณาเลือกรายงาน';
				$page = 'report';
			}

			$this->load->view('layout/engines/report/'.$page, $data);
		}
	}
}