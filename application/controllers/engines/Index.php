<?php
ob_start();

if (!defined('BASEPATH')) exit('No direct script access allowed');
//  echo error_reporting(-1);
session_start();

class Index extends CI_Controller
{	
	public $templateDir;
	public function __construct()
	{
		parent::__construct();

		$this->templateDir = 'layout/engines';
		$this->load->model('member_model', 'm_member');
		$this->load->library('session');
		$this->template->set_template('engines');
	}

	public function index($error=null)
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;

		if($this->session->userdata('logged_in')){
			redirect(base_url('engines/payment', 'refresh'));
		}else{
			$this->template->write_view('header', 'template/engines/header-top');
			$this->template->write_view('detail', $this->templateDir.'/home', $data, TRUE);
			$this->template->render();
		}	
	}

	public function login(){

		// echo 'dfdf';
		// exit();

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if (!empty($username) && !empty($password)) {
			$getMember = $this->m_member->getMember($username, $password);	
			// var_dump($getMember);
			if(!empty($getMember)){
				$sess_login = array('id' => $getMember[0]['id'], 'username' => $getMember[0]['username'], 'permission' => $getMember[0]['permission'], 'logged_in' => TRUE);
				$this->session->set_userdata('logged_in', $sess_login);

				// var_dump($this->session);
				

				// var_dump($this->session->userdata('logged_in'));
				// exit();
				// echo '<br>';
				// var_dump($this->session->all_userdata());
				$url = base_url('engines/payment');
				echo '<script>window.location.href = "'.$url.'";</script>';
				// redirect(base_url('/engines/payment', 'refresh'));
			}else{
				$error = 'Your Name or Password not match!!';
				$this->index($error); 
			}
		}
		// redirect(base_url('engines'));
	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect(base_url('engines'));
	}
}