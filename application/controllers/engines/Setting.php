<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 // echo error_reporting(-1);
session_start();

class Setting extends CI_Controller
{	
	public $templateDir;
	public function __construct()
	{
		parent::__construct();
		$this->templateDir = 'layout/engines';
		$this->load->model('payment_model', 'm_payment');
		$this->load->model('email_model', 'm_email');
		$this->template->set_template('engines');
		$this->load->library('mypagination', 'session');
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index()
	{
		// echo 'eeeeee';
		// exit();
		// if($this->session->userdata('logged_in'))
		// {
			// $session_data = $this->session->userdata('logged_in');

			// $data['fee'] = $this->m_payment->getFeeAll();

			// $paginate = $this->paginate($total_Employee, '1');
		// }else{
		// 	redirect(base_url('engines'));
		// }

		// $data['error'] = $error;
		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/setting/setting_list', $data, TRUE);
			$this->template->render();
		// }	
	}

	public function setting_email()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			// $setiing_id = $this->input->get('id', TRUE);
			$data['email'] = $this->m_email->getEmail();
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/setting/setting_email', $data, TRUE);
			$this->template->render();
		// }	
	}
	public function update_email()
	{
		// echo 'eeeeee';
		// exit();

		// $data['error'] = $error;

		$input = $this->input->post();
		foreach ($input as $key => $value){
			$data[$key] = $value;
		}

		if(!empty($input['id'])){
			$data['id'] = $input['id'];
			$data['state'] = $input['state'];
			$data['modified_date'] = date('Y-m-d H:s:i');
			$validate = $this->m_email->updateEmail($data);
		}else{
			$data['state'] = 1;
			$data['created_date'] = date('Y-m-d H:s:i');
			$validate = $this->m_email->setEmail($data);
		}

		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			// $setiing_id = $this->input->get('id', TRUE);
			
			echo '<script>alert("Update Success")</script>';
			echo '<script>window.location.href = "/engines/setting/setting/setting_email";</script>';
		// }	
	}

	public function setting_fee()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			$data['fee'] = $this->m_payment->getFeeAll();
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/setting/setting', $data, TRUE);
			$this->template->render();
		// }	
	}

	public function insert_fee()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/setting/setting_fee', $data, TRUE);
			$this->template->render();
		// }	
	}

	public function edit()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		// if($this->session->userdata('logged_in')){
		// 	redirect(base_url('engines/check_list', 'refresh'));
		// }else{
			$setiing_id = $this->input->get('id', TRUE);
			$data['fee'] = $this->m_payment->getFeeByID($setiing_id);
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/setting/setting_edit', $data, TRUE);
			$this->template->render();
		// }	
	}

	public function validation()
	{
		// if($this->session->userdata('logged_in'))
		// {
			// $session_data = $this->session->userdata('logged_in');
			$input = $this->input->post();
			// var_dump($input['payment_name']);
			// exit();
			$data['payment_name'] = $input['payment_name'];
			$data['percentage_fee'] = $input['percentage_fee'];
			$data['price_fee'] = $input['fee'];
			
			// $data['created_by'] = $session_data['username'];
			// var_dump($input['id']);
			// exit();
			if(!empty($input['id'])){
				$data['id'] = $input['id'];
				$data['state'] = $input['state'];
				$data['modified_date'] = date('Y-m-d H:s:i');
				$validate = $this->m_payment->updateFee($data);
			}else{
				$data['state'] = 1;
				$data['created_date'] = date('Y-m-d H:s:i');
				$validate = $this->m_payment->setFee($data);
			}
			
		
	        echo '<script>alert("Upload Success")</script>';
			echo '<script>window.location.href = "/engines/setting";</script>';
	    // }else{
	    // 	redirect(base_url('engines'));
	    // }
	}

	public function paginate($total_rows = null, $data_type = null, $keyword = null){

		$per_page = 20;

		$config['total_rows'] = count($total_rows);
		$config['per_page'] = $per_page;
		$config['base_url'] = base_url('engines/employee/index');
		$config["uri_segment"] = 4;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active">';
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		$data['page'] = $page;
		switch ($data_type) {
			case '1':
				$data['employee'] = $this->m_employee->getEmployeePage($page); 
				break;

			case '2':
				$data['employee'] = $this->m_employee->getSearch($keyword, $page); 
				break;
			
			default:
				$data['employee'] = $this->m_employee->getEmployeePage($page); 
				break;
		}

		$this->mypagination->initialize($config); 

		$data['pagination'] = $this->mypagination->create_links();
		$data['keyword'] = $keyword;

		$this->template->write_view('header', 'template/engines/header');
		$this->template->write_view('detail', $this->templateDir.'/setting/setting', $data , TRUE);
		$this->template->write_view('footer', 'template/engines/footer');
		$this->template->render();
	}
}