<?php
session_start();
if (!defined('BASEPATH')) exit('No direct script access allowed');
 // echo error_reporting(-1);
// session_start();

class Payment extends CI_Controller
{	
	public $templateDir;
	public function __construct()
	{
		parent::__construct();

		$this->templateDir = 'layout/engines';
		$this->load->model('payment_model', 'm_payment');
		// $this->load->model('member_model', 'm_member');
		$this->template->set_template('engines');
		$this->load->library('session');

		// $this->load->helper('form', 'url');
		// $this->load->library('mypagination', 'session');
	}

	public function index()
	{
		// $input = $this->input->post();

		// var_dump($input);

		// var_dump($this->session->userdata('logged_in'));
				// exit();
		

		// $data['error'] = $error;
		// var_dump($this->session);

		if($this->session->userdata('logged_in')){
			$total_invoice = $this->m_payment->getInvoicePage(0);
			$paginate = $this->paginate($total_invoice, '1');
		// 	redirect(base_url('engines/check_list', 'refresh'));
		}else{
			redirect(base_url('engines'));
			// $url = base_url('engines');
			// echo '<script>window.location.href = "'.$url.'";</script>';
			// $this->template->write_view('header', 'template/engines/header');
			// $this->template->write_view('detail', $this->templateDir.'/payment', $data, TRUE);
			// $this->template->render();
		}	
	}

	public function invoice()
	{
		// echo 'eeeeee';
		// exit();

		$data['error'] = $error;
		if(!$this->session->userdata('logged_in')){
			redirect(base_url('engines/payment', 'refresh'));
		}else{
			$hospital_no = $this->input->get('hospital_no', TRUE);
			$invoice_no = $this->input->get('invoice_no', TRUE);
			$bank_payment = $this->input->get('payment_channel', TRUE);
			if (!empty($bank_payment)) {
				$percentage_fee = $this->m_payment->getPrecentageFee($bank_payment);
			}
			if (!empty($hospital_no) && !empty($invoice_no)) {
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_PORT => "8295",
					CURLOPT_URL => "http://110.49.49.226:8295/prime/v2/inv",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					// CURLOPT_POSTFIELDS => "{\"INV\" : \"INV19100564\",\"HN\" : \"190343-F\"}",
					CURLOPT_POSTFIELDS => "{\"INV\" : \"" . $invoice_no . "\",\"HN\" : \"" . $hospital_no . "\"}",
					CURLOPT_HTTPHEADER => array(
						"Authorization: Basic cHJpbWVhcGk6NmIyQ2QlSiheYURs",
						"Content-Type: application/json",
					),
				));

				$response = curl_exec($curl);

				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
					$data['response'] = $err;
					// echo "cURL Error #:" . $err;
				} else {
					$json_response = json_decode($response);

					if ($json_response->status == 'Success') {
						$data['response'] = $json_response;
						$amount = $json_response->cus_info->PriceTotal;
						$fee_total = ($amount * $percentage_fee['0']->percentage_fee) / 100;
						$total = $amount + $fee_total;
						$discount = $json_response->cus_info->DiscountTotal;
						$grand_total = $total - $discount;
						$data['amount'] = $amount;
						$data['fee'] = $fee_total;
						$data['total'] = $total;
						$data['discount'] = $discount;
						$data['grand_total'] = $grand_total;
						$data['price_th'] = $this->price_th_convert(number_format($grand_total, 2));
						$data['percentage_fee'] = $percentage_fee['0']->percentage_fee;
					}
				}
			}
			$this->template->write_view('header', 'template/engines/header');
			$this->template->write_view('detail', $this->templateDir.'/payment/invoice', $data, TRUE);
			$this->template->render();
		}	
	}

	public function search(){
		// var_dump($this->input->post('search'));
		// exit();
		if($this->session->userdata('logged_in')){
			$keyword = trim($this->input->post('search'));
			// var_dump($keyword);
			// exit();
			if(!empty($keyword)){
				$search = $this->m_payment->getSearch($keyword);
				$paginate = $this->paginate($search, '2', $keyword);
			}else{
				echo '<script>window.location.href = "/engines/payment";</script>';
			}	
		}else{
	    	redirect(base_url('engines'));
	    }	
	}

	public function paginate($total_rows = null, $data_type = null, $keyword = null){
		$per_page = 20;

		$config['total_rows'] = count($total_rows);
		$config['per_page'] = $per_page;
		$config['base_url'] = base_url('engines/payment/index');
		$config["uri_segment"] = 4;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="active">';
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		$data['page'] = $page;
		switch ($data_type) {
			case '1':
				$data['payment_list'] = $this->m_payment->getInvoicePage($page); 
				break;

			case '2':
				$data['payment_list'] = $this->m_payment->getSearch($keyword, $page); 
				break;
			
			default:
				$data['payment_list'] = $this->m_payment->getInvoicePage($page); 
				break;
		}

		$this->mypagination->initialize($config); 

		$data['pagination'] = $this->mypagination->create_links();
		$data['keyword'] = $keyword;

		$this->template->write_view('header', 'template/engines/header');
		$this->template->write_view('detail', $this->templateDir.'/payment/payment', $data , TRUE);
		// $this->template->write_view('footer', 'template/engines/footer');
		$this->template->render();
	}

	public function price_th_convert($number = null)
	{

		$txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
		$txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
		$number = str_replace(",", "", $number);
		$number = str_replace(" ", "", $number);
		$number = str_replace("บาท", "", $number);
		$number = explode(".", $number);

		if (sizeof($number) > 2) {
			return 'ทศนิยมหลายตัวนะจ๊ะ';
			exit;
		}
		$strlen = strlen($number[0]);
		
		$convert = '';
		for ($i = 0; $i < $strlen; $i++) {
			$n = substr($number[0], $i, 1);			
			if ($n != 0) {
				if ($i == ($strlen - 1) and $n == 1) {
					$convert .= $txtnum1[$n];
				} elseif ($i == ($strlen - 1) and $n == 2) {
					$convert .= 'เอ็ด';
				} elseif ($i == ($strlen - 2) and $n == 2) {
					$convert .= 'ยี่';
				} elseif ($i == ($strlen - 2) and $n == 1) {
					$convert .= '';
				} else {
					$convert .= $txtnum1[$n];
				}
				$convert .= $txtnum2[$strlen - $i - 1];
			}
		}

		$convert .= 'บาท';
		if (
			$number[1] == '0' or $number[1] == '00' or
			$number[1] == ''
		) {
			$convert .= 'ถ้วน';
		} else {
			$strlen = strlen($number[1]);
			for ($i = 0; $i < $strlen; $i++) {
				$n = substr($number[1], $i, 1);
				if ($n != 0) {
					if ($i == ($strlen - 1) and $n == 1) {
						$convert
							.= 'เอ็ด';
					} elseif (
						$i == ($strlen - 2) and
						$n == 2
					) {
						$convert .= 'ยี่';
					} elseif (
						$i == ($strlen - 2) and
						$n == 1
					) {
						$convert .= '';
					} else {
						$convert .= $txtnum1[$n];
					}
					$convert .= $txtnum2[$strlen - $i - 1];
				}
			}
			$convert .= 'สตางค์';
		}
		return $convert;
		// } 
		## วิธีใช้งาน
		// $x = '9123568543241.25'; 
		// echo  $x  . "=>" .convert($x); 

	}
}