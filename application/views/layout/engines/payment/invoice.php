<link href="<?php echo base_url('assets/home/css/style.css');?>" rel="stylesheet">
<?php
// var_dump($response);
$customer_info = $response->cus_info;
$invoice_info = $response->inv_detail;
if ($response->status != "Success") {
?>
	<div class="container">
		<div class="desc">
			<div class="row">
				<div class="col-md-12 invoice_head">
					<div class="col-md-2">
						<img src="<?php echo base_url('assets/home/img/logo.jpg'); ?>">
					</div>
					<div class="col-md-10">
						<h5><b>Prime Fertility Center Co.,Ltd.</b></h5>
						<p>1177 Pearl Bangkok Building, 22nd Fl., Phahonyothin Rd., Phayathai, Bangkok 10400</p>
						<p>Tel: (+66)02-029-1418-9, www.primefertilitycenter.com</p>
						<p>Tax ID: 0-105-561-06012-0</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<br><br><br>
					<h3><b>ไม่พบข้อมูลที่ค้นหา <br> Data Not Found Please Check Data Again.</b></h3>
				</div>
			</div>
		</div>
	</div>
<?php
} else {
?>
	<div class="container">
		<div class="desc">
			<div class="row">
				<div class="col-md-12 invoice_head">
					<div class="col-md-2">
						<img src="<?php echo base_url('assets/home/img/logo.jpg'); ?>">
					</div>
					<div class="col-md-10">
						<h5><b>Prime Fertility Center Co.,Ltd.</b></h5>
						<p>1177 Pearl Bangkok Building, 22nd Fl., Phahonyothin Rd., Phayathai, Bangkok 10400</p>
						<p>Tel: (+66)02-029-1418-9, www.primefertilitycenter.com</p>
						<p>Tax ID: 0-105-561-06012-0</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 invoice_body">
					<h5><b>ใบแจ้งค่าบริการ/INVOICE/发票</b></h5>
					<div class="col-md-12 invoice">
						<div class="col-md-6">
							<p><b>ชื่อ/Patient Name/病者的姓名 : </b><?php echo $customer_info->PatientName; ?></p>
							<p><b>เลขประจำตัวผู้ป่วย/Patient ID/病人号 : </b><?php echo $customer_info->HN; ?></p>
							<p><b>แพทย์/Doctor/医生 : </b><?php echo $customer_info->DoctorName; ?></p>
						</div>
						<div class="col-md-6">
							<p><b>วันที่/Date/日期 : </b><?php echo explode(' ', $customer_info->DocDate)[0]; ?></p>
							<p><b>เลขที่ใบแจ้งหนี้/Invoice No./发票号码 : </b><?php echo $customer_info->SaleNumber; ?></p>
						</div>
					</div>
					<div class="col-md-12" style="padding:0px; font-size:14px;">
						<table class="table">
							<thead class="thead-light">
								<tr>
									<th scope="col" width="8%" class="center">ลำดับ<br>顺序号</th>
									<th scope="col" width="10%" class="center">รหัสสินค้า<br>产品代码</th>
									<th scope="col" width="25%" class="center">รายการ<br>项目</th>
									<th scope="col" width="5%" class="center">จำนวนหน่วย<br>单位</th>
									<th scope="col" width="8%" class="center">ราคา/หน่วย<br>单价</th>
									<th scope="col" width="8%" class="center">ส่วนลด<br>折扣</th>
									<th scope="col" width="10%" class="center">ยอดเงิน<br>金额</th>
									<th scope="col" width="10%" class="center">ยอดหัก<br>แพ็กเกจ<br>不含套餐<br>的费用</th>
									<th scope="col" width="10%" class="center">จำนวนเงิน<br>金额</th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i = 0; $i < count($invoice_info); $i++) { ?>
									<tr>
										<td scope="row"><?php echo $i + 1; ?></td>
										<td><?php echo $invoice_info[$i]->Prod_Code; ?></td>
										<td><?php echo $invoice_info[$i]->ProdName; ?></td>
										<td class="center">
											<?php echo number_format($invoice_info[$i]->ShowQty); ?>
										</td>
										<td class="right">
											<?php echo number_format($invoice_info[$i]->ShowPricePerUnit, 2); ?>
										</td>
										<td class="right">
											<?php
											if ($invoice_info[$i]->Discount == 0) {
												echo '-';
											} else {
												echo number_format($invoice_info[$i]->Discount, 2);
											}
											?>
										</td>
										<td class="right">
											<?php
											if ($invoice_info[$i]->Total == 0) {
												echo '-';
											} else {
												echo number_format($invoice_info[$i]->Total, 2);
											}
											?>
										</td>
										<td class="right">
											<?php
											if ($invoice_info[$i]->Total_Package == 0) {
												echo '-';
											} else {
												echo number_format($invoice_info[$i]->Total_Package, 2);
											}
											?>
										</td>
										<td class="right">
											<?php
											if ($invoice_info[$i]->Total_Net == 0) {
												echo '-';
											} else {
												echo number_format($invoice_info[$i]->Total_Net, 2);
											}
											?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5"><b>หมายเหตุ/Remark/备注 :</b> <?php echo $customer_info->Remark; ?></td>
									<td colspan="3"><b>ยอดเงินรวม/Amount/总金额 :</b></td>
									<td class="right">
										<?php
										if ($amount == 0) {
											echo '-';
										} else {
											echo number_format($amount, 2);
										}
										?>
									</td>
								</tr>
								<tr>
									<td colspan="5"></td>
									<td colspan="3"><b>ค่าธรรมเนียม/Fee/手续费 <?php echo $percentage_fee . '% '; ?>:</b></td>
									<td class="right">
										<?php
										if ($fee == 0) {
											echo '-';
										} else {
											echo number_format($fee, 2);
										}
										?>
									</td>
								</tr>
								<tr>
									<td colspan="5"></td>
									<td colspan="3"><b>รวม/Total/合计 :</b></td>
									<td class="right">
										<?php
										if ($total == 0) {
											echo '-';
										} else {
											echo number_format($total, 2);
										}
										?>
									</td>
								</tr>
								<tr>
									<td colspan="5"></td>
									<td colspan="3"><b>ส่วนลด/Discount/折扣 :</b></td>
									<td class="right">
										<?php
										if ($discount == 0) {
											echo '-';
										} else {
											echo number_format($discount, 2);
										}
										?>
									</td>
								</tr>
								<tr>
									<td colspan="5" class="center">(<?php echo $price_th; ?>)</td>
									<td colspan="3"><b>รวมทั้งสิ้น/Grand Total/共计 :</b></td>
									<td class="right">
										<?php
										if ($grand_total == 0) {
											$grand = $total;
										} else {
											$grand = $grand_total;
										}
										echo number_format($grand, 2);
										?>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".payment")).addClass("active");
	});
</script>