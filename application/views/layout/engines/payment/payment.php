<div class="payment mgt20">
	<div class="container">
		<div class="row mgt20">
			<div class="col-md-12">
				<h3>Invoice</h3>
				<form class="form-inline mgt20" action="<?php echo base_url('engines/payment/search');?>" method="post" enctype="multipart/form-data">
					<div class="form-group mb-2">
						<input type="text" class="form-control" id="search" name="search" placeholder="invoice_no">
					</div>
					<button type="submit" class="btn btn-primary mx-sm-3 mb-2">Search</button>
				</form>
			</div>
		</div>
		<div class="row">
			<table class="table payment">
			  <thead>
			    <tr>
			      <th scope="col">No.</th>
			      <th scope="col">Hospital Number</th>
			      <th scope="col">Invoice No.</th>
			      <th scope="col">Amount</th>
			      <th scope="col">Channel</th>			      
			      <th scope="col">Status</th>
			      <th scope="col">Payment Date</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php  //var_dump($payment_list);
			  	if(!empty($payment_list)){
			  		$row = 1;
			  		for ($i=0; $i<count($payment_list); $i++) { 
			  			if($payment_list[$i]->payment_status == '1'){
			  				$status = 'Waiting Payment';
			  				$color = 'red';
			  				$payment_date = $payment_list[$i]->created_date; 
					    }else{
			  				$status = 'Paid';
			  				$color = 'green'; 
			  				$payment_date = $payment_list[$i]->payment_date;
						}
						$grand_total = $payment_list[$i]->amount+$payment_list[$i]->fee-$payment_list[$i]->discount;
			  			echo '<tr>
					      <td scope="row">'.$row.'</td>
					      <td>'.$payment_list[$i]->hospital_number.'</td>
					      <td><a href="'.base_url('engines/payment/invoice?hospital_no='.$payment_list[$i]->hospital_number.'&invoice_no='.$payment_list[$i]->invoice_no.'&payment_channel='.$payment_list[$i]->payment_channel).'">'.$payment_list[$i]->invoice_no.'</a></td>
					      <td style="text-align: right;">'.number_format($grand_total, 2).'</td>
					      <td>'.$payment_list[$i]->chanel['0']->payment_name.'</td>
					      <td style="color:'.$color.'">'.$status.'</td>
						  <td>'.$payment_date.'</td>
						</tr>';
					$row++;
			  		}
			  	}
			  	?>
			  </tbody>
			</table>
		</div>
	</div>
	<div class="margin20">
	    <ul class="pagination">
	        <?php echo $pagination;?>
	    </ul>    
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".payment")).addClass("active");
	});
</script>