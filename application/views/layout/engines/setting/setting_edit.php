<div class="payment mgt20">
	<div class="container">
		<form class="form mgt20" action="<?php echo base_url('engines/setting/validation');?>" method="post" name="admin" id="admin">
			<div class="row mgt20">
				<div class="col-md-12 dpf">
					<div class="col-md-10">
						<h3>Setting Fee</h3>
					</div>
					<div class="col-md-2">
						<button class="btn btn-success" type="submit">Save</button>
						<a href="<?php echo base_url('engines/setting'); ?>" class="btn btn-danger">Close</a>
					</div>
				</div>
			</div>
			<div class="row mgt20">
				<div class="col-md-12">
					<div class="col-md-6">
						<label>Payment Name Fee: *</label>
						<input type="text" name="payment_name" placeholder="Payment Name Fee" class="form-control" value="<?php echo $fee['0']->payment_name;?>">
						<label>Percentage Fee: *</label>
						<select name="percentage_fee" class="form-control">
							<option>Select Percentage Fee</option>
							<?php for ($i=1; $i<=10; $i++) {
								if(!empty($fee['0']->percentage_fee) && $i == $fee['0']->percentage_fee){
									echo '<option value='. $fee['0']->percentage_fee.' selected>'. $fee['0']->percentage_fee .'% </option>';
								}else{
									echo '<option value='. $i.'>'. $i .'% </option>';
								}								
							} ?>
						</select>
						<label>Amount Fee: </label>
						<input type="text" name="fee" placeholder="Amount Fee" class="form-control" value="<?php echo $fee['0']->price_fee;?>">
						<label>Status: </label>
						<?php if($fee['0']->state == 1){ 
							$checkopen = 'checked="checked"';
							$checkclose = '';
						}else{ 
							$checkopen = '';
							$checkclose = 'checked="checked"';
						} ?>
						<input type="radio" name="state" value="1" <?php echo $checkopen;?>> Open
						<input type="radio" name="state" value="-2" <?php echo $checkclose;?>> Close
						
						<input type="hidden" name="id" value="<?php echo $fee['0']->id;?>">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>