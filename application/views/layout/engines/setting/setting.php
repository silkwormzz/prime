<div class="payment mgt20">
	<div class="container">
		<div class="row mgt20">
			<div class="col-md-12 dpf">
				<div class="col-md-10">
					<h3>Setting Fee</h3>
				</div>
				<div class="col-md-2">
					<a href="<?php echo base_url('engines/setting/insert_fee'); ?>" class="btn btn-primary">Add Fee</a>
				</div>
			</div>
		</div>
		<?php //var_dump($fee); ?>
		<div class="row mgt20">
			<table class="table payment">
			  <thead>
			    <tr>
			      <th scope="col">No.</th>
			      <th scope="col">Name</th>
			      <th scope="col">Percentage</th>
			      <th scope="col">Price</th>
			      <th scope="col">Status</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $row = 1;
			  	for($i=0; $i<count($fee); $i++){ ?>
				    <tr>
						<td scope="row"><?php echo $row;?></td>
						<td>
							<a href="<?php echo base_url('engines/setting/edit?id='.$fee[$i]->id);?>">
								<?php echo $fee[$i]->payment_name;?>
							</a>
						</td>
						<td>
							<?php 
							if($fee[$i]->percentage_fee == '0'){
								echo '';
							}else{
								echo $fee[$i]->percentage_fee.'%';
							}				      	
							?>
							</td>
						<td>
					      	<?php 
					      	if($fee[$i]->price_fee == '0.00'){
					      		echo '';
					      	}else{
								echo $fee[$i]->price_fee;
					      	}				      	
					      	?>
				      	</td>
				      	<td>
					      	<?php 
					      	if($fee[$i]->state == '1'){
					      		echo '<span style="color:green;">Open</span>';
					      	}else{
								echo '<span style="color:red;">Close</span>';
					      	}				      	
					      	?>
				      	</td>
				    </tr>
			    <?php $row++;
			    } ?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>