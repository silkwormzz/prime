<div class="payment mgt20">
	<div class="container">
		<form class="form mgt20" action="<?php echo base_url('engines/setting/update_email'); ?>" method="post" name="admin" id="admin">
			<div class="row mgt20">
				<div class="col-md-12 dpf">
					<div class="col-md-10">
						<h3>Setting Email</h3>
					</div>
					<div class="col-md-2">
						<button class="btn btn-success" type="submit">Save</button>
						<a href="<?php echo base_url('engines/setting'); ?>" class="btn btn-danger">Close</a>
					</div>
				</div>
			</div>
			<div class="row mgt20">
				<div class="col-md-12">
					<div class="col-md-6">
						<label>SMTP : *</label>
						<input type="text" name="smtp" placeholder="smtp.domain.com" class="form-control" value="<?php echo $email['0']->smtp; ?>">
						<label>Port : *</label>
						<input type="text" name="port" placeholder="25" class="form-control" value="<?php echo $email['0']->port; ?>">
						<label>Secure : *</label>
						<select name="secure" class="form-control">
							<option>Select Secure</option>
							<?php
							if (!empty($email['0']->secure)) {
								if($email['0']->secure == 0){
									$select_none = 'selected'; 
									$select_ssl = '';
									$select_tls = '';
								}elseif($email['0']->secure == 1){
									$select_none = ''; 
									$select_ssl = 'selected';
									$select_tls = '';
								}
								elseif($email['0']->secure == 2){
									$select_none = ''; 
									$select_ssl = '';
									$select_tls = 'selected';
								}
							?>
							
								<option value=0 <?php echo $select_none?>>NONE</option>
								<option value=1 <?php echo $select_ssl?>>SSL</option>
								<option value=2 <?php echo $select_tls?>>TLS</option>
							<?php
							} else {
								echo '<option value=0>NONE</option>';
								echo '<option value=1>SSL</option>';
								echo '<option value=2>TLS</option>';
							}
							?>
						</select>
						<label>Name : *</label>
						<input type="text" name="from_name" placeholder="Firstname Lastname" class="form-control" value="<?php echo $email['0']->from_name; ?>">
						<label>From : *</label>
						<input type="text" name="from" placeholder="email@domail.com" class="form-control" value="<?php echo $email['0']->from; ?>">
						<label>To : *</label>
						<input type="text" name="to" placeholder="email@domail.com" class="form-control" value="<?php echo $email['0']->to; ?>">
						<label>CC : *</label>
						<input type="text" name="cc" placeholder="email@domail.com" class="form-control" value="<?php echo $email['0']->cc; ?>">
						<label>Username : *</label>
						<input type="text" name="username" placeholder="email@domail.com" class="form-control" value="<?php echo $email['0']->username; ?>">
						<label>Password : *</label>
						<input type="password" name="password" placeholder="**********" class="form-control" value="<?php echo $email['0']->password; ?>">
						<label>Status: </label>
						<?php if ($email['0']->state == 1) {
							$checkopen = 'checked="checked"';
							$checkclose = '';
						} else {
							$checkopen = '';
							$checkclose = 'checked="checked"';
						} ?>
						<input type="radio" name="state" value="1" <?php echo $checkopen; ?>> Open
						<input type="radio" name="state" value="-2" <?php echo $checkclose; ?>> Close
						<input type="hidden" name="id" value="<?php echo $email['0']->id; ?>">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>