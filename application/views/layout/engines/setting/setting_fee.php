<div class="payment mgt20">
	<div class="container">
		<form class="form mgt20" action="<?php echo base_url('engines/setting/validation');?>" method="post" name="admin" id="admin">
			<div class="row mgt20">
				<div class="col-md-12 dpf">
					<div class="col-md-10">
						<h3>Setting Fee</h3>
					</div>
					<div class="col-md-2">
						<button class="btn btn-success" type="submit">Save</button>
						<a href="<?php echo base_url('engines/setting'); ?>" class="btn btn-danger">Close</a>
					</div>
				</div>
			</div>
			<div class="row mgt20">
				<div class="col-md-12">
					<div class="col-md-6">
						<label>Payment Name Fee: *</label>
						<input type="text" name="payment_name" placeholder="Payment Name Fee" class="form-control">
						<label>Percentage Fee: *</label>
						<select name="percentage_fee" class="form-control">
							<option>Select Percentage Fee</option>
							<?php for ($i=1; $i<=10; $i++) {
								echo '<option value='. $i.'>'. $i .'% </option>';
							} ?>
						</select>
						<label>Amount Fee: </label>
						<input type="text" name="fee" placeholder="Amount Fee" class="form-control">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>