<div class="payment mgt20">
	<div class="container">
		<div class="row mgt20">
			<div class="col-md-12 dpf">
				<div class="col-md-10">
					<h3>Setting List</h3>
				</div>
			</div>
		</div>
		<div class="row mgt20">
			<div class="col-md-12">
				<a href="<?php echo base_url('engines/setting/setting_fee');?>">
					<p><i class="fa fa-arrow-right"></i> Fee</p>
				</a>
				<a href="<?php echo base_url('engines/setting/setting_email');?>">
					<p><i class="fa fa-arrow-right"></i> Email</p>					
				</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>