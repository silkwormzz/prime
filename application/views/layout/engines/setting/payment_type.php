<div class="payment mgt20">
	<div class="container">
		<form class="form mgt20" name="admin" id="admin">
			<div class="row mgt20">
				<div class="col-md-12 dpf">
					<div class="col-md-10">
						<h3>Setting Payment Type</h3>
					</div>
					<div class="col-md-2">
						<a href="<?php echo base_url('engines/setting/setting_fee'); ?>" class="btn btn-success">Save</a>
						<a href="<?php echo base_url('engines/setting'); ?>" class="btn btn-danger">Close</a>
					</div>
				</div>
			</div>
			<div class="row mgt20">
				<div class="col-md-12">
					<div class="col-md-6">
						<label>Name: </label>
						<input type="text" name="name" placeholder="Name" class="form-control">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".setting")).addClass("active");
	});
</script>