<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Content-type: application/vnd-ms-excel"); 
header("Content-Disposition: attachment; filename=ReportPaymentComplate.xls"); 
header("Pragma: no-cache");
header('Cache-Control: max-age=0');
header("Expires: 0");
date_default_timezone_set("Asia/Bangkok");

if(!empty($start_date) || !empty($end_date)){
  $start = $start_date;
  $end = $end_date;
}else{
  $start = date('Y-m-d H:i:s');
  $end = date('Y-m-d H:i:s');
}
// var_dump($search);
?>
<html lang="en">
 	<head>
	    <meta charset="utf-8">
	</head> 
 	<body>
	    <main>
	    	<div style="text-align:center; font-family:Browallia New; font-size:24px;">
				<p><b>รายงานการจ่ายเงินสำเร็จ</b></p>
				<p><b>ประจำวันที่ <?php echo $start;?> ถึงวันที่ <?php echo $end;?></b></p>				
			</div>	           
	      	<table border="1" width="100%">
		        <thead>
		            <tr>
		              <th></th>
		              <th style="text-align: center;">Invoice No</th>
		              <th style="text-align: center;">Hospital Number</th>
		              <th style="text-align: center;">Amount</th>
		              <th style="text-align: center;">Fee</th>
		              <th style="text-align: center;">Total</th>
		              <th style="text-align: center;">Payment Channel</th>
		              <th style="text-align: center;">Payment Status</th>
		              <th style="text-align: center;">Payment Date</th>
		            </tr>
		        </thead>
		        <tbody>
		          	<?php for ($i=0; $i<count($data); $i++) { 
		          		$total_amount += $data[$i]->amount;
		          		$total_fee += $data[$i]->fee;
		          		$total_total += $data[$i]->total;
		          	?>
		            <tr>
						<td style="text-align: center;"><?php echo $i + 1;?></td>
						<td style="text-align: center;"><?php echo $data[$i]->invoice_no; ?></td>
						<td style="text-align: center;"><?php echo $data[$i]->hospital_number; ?></td>
						<td style="text-align: right;"><?php echo number_format($data[$i]->amount, 2); ?></td>
						<td style="text-align: right;"><?php echo number_format($data[$i]->fee, 2); ?></td>
						<td style="text-align: right;"><?php echo number_format($data[$i]->total, 2); ?></td>
						<td><?php echo $data[$i]->chanel['0']->payment_name; ?></td>
						<td style="text-align: center;">
							<?php if($data[$i]->payment_status == "2"){
							echo 'Paid';
							}else{
							echo 'Waiting Payment';
							}  
							?>                          
						</td>
						
						<td style="text-align: center;">
						<?php echo '.'.explode(' ', $data[$i]->payment_date)['0']; ?>
						</td>
		            </tr>
		          <?php } ?>
		        </tbody>
		        <tfoot>
		        	<td colspan="3" style="text-align: right;">Total</td>
		        	<td style="text-align: right;"><?php echo number_format($total_amount, 2);?></td>
		        	<td style="text-align: right;"><?php echo number_format($total_fee, 2);?></td>
		        	<td style="text-align: right;"><?php echo number_format($total_total, 2);?></td>
		        	<td colspan="3"></td>
		        </tfoot>
	      	</table>
		</main>
 	</body>
</html>