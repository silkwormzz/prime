<div class="payment mgt20">
	<div class="container">
		<?php if(!empty($error)){
			echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
		}?>
		<div class="row mgt20">			
			<div class="col-md-12">
				<h3>Report</h3>
			</div>
			<div class="col-md-12">
				<form action="<?php echo base_url('engines/report/reports');?>" method="post" enctype="multipart/form-data">
					<div class="col-md-12 form-group form-inline" style="margin-top: 50px;">
						<div>
							<input type="text" class="form-control" name="start_date" id="start_date" placeholder="Start Date" autocomplete="off" value="<?php echo $start_date;?>">
							<i class="fa fa-calendar" id="i_start_date"></i>	
						</div>
						<div style="margin-left: 30px;">
							<input type="text" class="form-control" name="end_date" id="end_date" placeholder="End Date" autocomplete="off" value="<?php echo $end_date;?>">
							<i class="fa fa-calendar" id="i_end_date"></i>
						</div>	
						<div style="margin-left: 30px;">
							<!-- <button type="submit" class="btn btn-primary">ออกรายงาน</button> -->
							<button type="submit" class="btn btn-primary">รายงาน(Excel)</button>
						</div>					
					</div>
					
					<div class="col-md-6" style="margin-top: 30px;">
						<input type="radio" name="report" value="1"> รายงานการจ่ายเงินทั้งหมด <br>
						<input type="radio" name="report" value="2"> รายงานการจ่ายเงินสำเร็จ <br>
						<input type="radio" name="report" value="3"> รายงานการจ่ายเงินไม่สำเร็จ <br>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#side-menu li").find(jQuery(".report")).addClass("active");    
	    jQuery("#start_date").datepicker({ dateFormat: 'yy-mm-dd' });
	    jQuery("#end_date").datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery("#i_start_date").on("click", function(){	      	
		   	jQuery("#start_date").datepicker('show');		    
	    });
	    jQuery("#i_end_date").on("click", function(){	      	
		   	jQuery("#end_date").datepicker('show');		    
	    });
	});
</script>