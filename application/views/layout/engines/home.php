		<div class="container">      
      <form class="form-signin" action="<?php echo base_url('engines/index/login');?>" method="post">
        <img src="<?php echo base_url('assets/home/img/logo.jpg'); ?>">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php
          if($error != NULL){
            echo '<p style="color:red"><b>'.$error.'</b></p>';
          }
        ?>
        <label for="inputEmail" class="sr-only">Name</label>
        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div> 
    <!-- /container -->