﻿<section id="whyUs">
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="whyus_top">
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <h4><a style="color: #273085;" href="https://www.primefertilitycenter.com/"><i class="fa fa-home"></i>HOME</a></h4>
            </div>
          </div>
          <form class="form-signin" action="<?php echo base_url('/payment/invoice'); ?>" method="post" id="admin" name="admin">
            <img class="img-fluid" src="<?php echo base_url('assets/home/img/logo.png'); ?>">
            <!-- <h2 class="form-signin-heading text-center">Prime Fertility Center</h2> -->
            <br><br>
            <h2 class="form-signin-heading text-center">Prime 生殖中心</h2>
            <?php
            if ($error != NULL) {
              echo '<p style="color:red"><b>' . $error . '</b></p>';
            }
            ?>

            <label for="hospital_no" class="label-home text-center"><b>เลขประจำตัวผู้ป่วย/Patient ID/病人号</b></label>
            <input type="text" class="form-control form-control-lg" name="hospital_no" placeholder="190000-F" required autofocus>
            <label for="invoice_no" class="label-home text-center"><b>เลขที่ใบแจ้งหนี้/Invoice No./发票号码</b></label>
            <input type="text" name="invoice_no" class="form-control form-control-lg" placeholder="INV19000000" required>
            <div class="form-group">
              <label for="bank_payment" class="label-home text-center"><b>วิธีการชำระเงิน/Select Payment/选择付款</b></label>

              <select class="form-control form-control-lg" name="bank_payment" required>
                <option value="">--Payment Channel--</option>
                <?php
                if (!empty($payment_channel)) {
                  for ($i = 0; $i < count($payment_channel); $i++) {
                    echo '<option value=' . $payment_channel[$i]->id . '>' . $payment_channel[$i]->payment_name . '</option>';
                  }
                } else {
                  echo '<option>Data Not Found.</option>';
                }
                ?>
              </select>
            </div>
            <div class="slide-submit">
              <div class="slide-submit-text">Slide To Submit / 滑动以确认</div>
              <div class="slide-submit-thumb">Send / 发送 »</div>
            </div>
            <div style="display: none;">
              <input type="submit" name="submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  jQuery(document).ready(function() {
    jQuery('.slide-submit').slideToSubmit({
      errorText: 'Check required fields', // Shown if fields are invalid & browser doesn't have built-in tooltips
      successText: 'Loading...', // Shown before submitting
      submitDelay: 100, // Delay for showing successText
      graceZone: 100 // Pixels from the right that is accepted as a full side

    });
    $('#admin').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });
  });
</script>