<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/home/img/favicon.ico');?>">

    <title>Prime Fertility Center</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo base_url('assets/engines/css/signin.css');?>" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://tinymce.cachefly.net/4.3/tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  </head>

  <body>
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="navbar-header">
          <a class="navbar-brand" href="#">Prime Fertility Center</a>
      </div>
      <!-- /.navbar-header -->            

      <div class="navbar-default sidebar" role="navigation">
          <!-- <div class="sidebar-nav navbar-collapse"> -->
        <?php var_dump($this->session->userdata('logged_in')['permission']); ?>
          <ul class="nav" id="side-menu">
            <li class="nav-item active">
              <a href="<?php echo base_url('engines/payment');?>" class="nav-link payment">
                Payment List
              </a>
            </li>
            <?php if($this->session->userdata('logged_in')['permission'] == '1'){ ?>
              <li class="nav-item active">
                  <a href="<?php echo base_url('engines/report');?>" class="nav-link report">
                    Report
                  </a>
              </li>
              <li class="nav-item active">
                  <a href="<?php echo base_url('engines/setting');?>" class="nav-link setting">
                    Setting
                  </a>
              </li>
            <?php } ?>                 
            <li>
                <a href="<?php echo base_url('engines/index/logout');?>" class="nav-link">
                  Logout
                </a>
            </li>                 
          </ul>
          <!-- </div> -->
          <!-- /.sidebar-collapse -->
      </div>
      <!-- /.navbar-static-side -->
  </nav>