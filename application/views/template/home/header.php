<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>
      <?php 
        if (isset($title)) {
          echo $title;
        } else {
          echo "Prime Fertility Center";
        }        
      ?>       
     </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="<?php echo base_url('assets/home/img/favicon.ico');?>"/>
    <!-- CSS
    ================================================== -->       
    <!-- Bootstrap css file-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/home/css/slide-to-submit.css');?>">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home/js/slide-to-submit.js');?>"></script>
    <!-- Font awesome css file-->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/home/css/style.css');?>" rel="stylesheet">
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=K2D&display=swap" rel="stylesheet">
  </head>
  <body> 
    <!--=========== BEGIN HEADER SECTION ================-->
    <header id="header">
      <!-- BEGIN MENU -->
      <div class="menu_area">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">  <div class="container">
            <div class="navbar-header">            
              <!-- IMG BASED LOGO  -->
              <a class="navbar-brand" href="<?php echo base_url('');?>">
                <img src="<?php echo base_url('assets/home/img/logo-light130x80.png');?>" alt="logo" class="logo_img">
              </a>   
            </div>
          </div>     
        </nav>  
      </div>
      <!-- END MENU -->    
    </header>
    <!--=========== END HEADER SECTION ================--> 