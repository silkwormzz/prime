<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getEmail()
	{
		$this->db->select('*');
		$query = $this->db->get('email_setting');
		// var_dump($query->num_rows());
		// exit();
		
		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else        
		{
			return false;
		}
    }
    public function setEmail($data = null)
	{
		$query = $this->db->insert('email_setting', $data);
		$last_id = $this->db->insert_id();
		return $last_id;
    }
    public function updateEmail($data = null)
	{
		$id = $data['id'];
		$query = $this->db->update('email_setting', $data, array('id' => $id));
		return $query;
	}
}