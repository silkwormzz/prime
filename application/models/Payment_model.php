<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function setInvoice($data = null)
	{
		// var_dump($data);
		if (!empty($data)) {
			// $this->db->where("state", "1");
			// $this->db->where("invoice_no", $data['invoice_no']);
			// $query = $this->db->get('invoice');
			// $invoice = $query->result();

			$invoice = $this->getInvoiceByNo($data['invoice_no']);

			// var_dump($invoice);

			// exit();

			if (empty($invoice)) {
				$query = $this->db->insert('invoice', $data);
				$invoice_id = $this->db->insert_id();
				// return $last_id;
			} else {
				// 	$this->updateInvoice($data['invoice_no'], $invoice['id']);
				$invoice_id = $invoice['0']->id;
			}

			$query = $this->db->insert('invoice_log', $data);
			// $this->db->insert_id();

			if ($data['payment_channel'] == 3) {
				$credit_card = $this->credit_card_channel($data, $invoice_id);
				return $credit_card;
			} elseif ($data['payment_channel'] == 4) {
				$credit_card = $this->credit_card_channel($data, $invoice_id);
				return $credit_card;
			} elseif ($data['payment_channel'] == 1) {
				$credit_card = $this->credit_card_channel($data, $invoice_id);
				return $credit_card;
			}
		}
	}

	public function updateInvoice($id = null, $invoice = null, $payment_status_note = null)
	{
		if ($payment_status_note == '000') {
			$update['payment_status'] = '2';
			$update['payment_date'] = date('Y-m-d H:s:i');
			$update['modified_date'] = date('Y-m-d H:s:i');
		} else {
			$update['payment_status'] = '1';
			$update['modified_date'] = date('Y-m-d H:s:i');
		}

		$query = $this->db->update('invoice', $update, array('id' => $id));

		// var_dump($invoice);
		$insert_log['invoice_no'] = $invoice['0']->invoice_no;
		$insert_log['hospital_number'] = $invoice['0']->hospital_number;
		$insert_log['amount'] = $invoice['0']->amount;
		$insert_log['fee'] = $invoice['0']->fee;
		$insert_log['total'] = $invoice['0']->total;
		$insert_log['payment_channel'] = $invoice['0']->payment_channel;
		$insert_log['state'] = $invoice['0']->state;
		$insert_log['created_date'] = $invoice['0']->created_date;
		$update['payment_status_note'] = $payment_status_note;


		$query = $this->db->insert('invoice_log', $insert_log);
		$log_id = $this->db->insert_id();
		$query = $this->db->update('invoice_log', $update, array('id' => $log_id));

		return $query;
	}

	public function insertInvoice($payment_status_note = null)
	{
		if ($payment_status_note == '000') {
			$update['payment_status'] = '2';
		} else {
			$update['payment_status'] = '1';
		}


		$insert_log['invoice_no'] = '0123456789';
		$insert_log['hospital_number'] = '123456789';
		$insert_log['amount'] = '100';
		$insert_log['fee'] = '10';
		$insert_log['total'] = '110';
		$insert_log['payment_channel'] = '1';
		$insert_log['state'] = '1';
		$insert_log['created_date'] = date('Y-m-d H:s:i');
		$insert_log['payment_status_note'] = $payment_status_note;


		$query = $this->db->insert('invoice_log', $insert_log);
		$log_id = $this->db->insert_id();
		// $query = $this->db->update('invoice_log', $update, array('id' => $log_id));

		return $query;
	}

	public function getInvoiceByNo($invoice_no = null)
	{
		$this->db->where("state", "1");
		$this->db->where("invoice_no", $invoice_no);
		$query = $this->db->get('invoice');
		return $query->result();
	}

	public function getInvoiceByID($invoice_id = null)
	{
		$this->db->where("state", "1");
		$this->db->where("id", $invoice_id);
		$query = $this->db->get('invoice');
		return $query->result();
	}

	public function getInvoicePage($start = null)
	{
		$this->db->where("state", "1");
		$this->db->order_by("id", "desc");
		$this->db->limit('20', $start);
		$query = $this->db->get('invoice');

		$invoice = $query->result();
		
		foreach ($invoice as $key => $value) {
			$value->chanel = $this->getFeeByID($value->payment_channel);
		}

		return $query->result();
	}

	public function setFee($data = null)
	{
		$query = $this->db->insert('base_fee', $data);
		$last_id = $this->db->insert_id();
		return $last_id;
	}

	public function getFee()
	{
		$this->db->where("state", "1");
		$query = $this->db->get('base_fee');

		return $query->result();
	}

	public function getFeeAll()
	{
		// $this->db->where("state", "1");
		$query = $this->db->get('base_fee');

		return $query->result();
	}

	public function getFeeByID($setting_id = null)
	{ 
		// $this->db->where("state", "1");
		$this->db->where("id", $setting_id);
		$query = $this->db->get('base_fee');

		return $query->result();
	}

	public function updateFee($data = null)
	{
		$id = $data['id'];
		$query = $this->db->update('base_fee', $data, array('id' => $id));

		// $query = $this->db->get('base_fee');

		return $query;
	}

	public function getPrecentageFee($fee_id)
	{
		// $this->db->where("state", "1");
		$this->db->where("id", $fee_id);
		$query = $this->db->get('base_fee');

		return $query->result();
	}

	public function getSearch($search = null)
	{
		$this->db->where("state", "1");
		$this->db->like("invoice_no", $search, 'both');
		$query = $this->db->get('invoice');

		$invoice = $query->result();
		
		foreach ($invoice as $key => $value) {
			$value->chanel = $this->getFeeByID($value->payment_channel);
		}

		return $query->result();
	}

	private function credit_card_channel($data = null, $invoice_id = null)
	{

		if ($data['payment_channel'] == 3) {
			$payment['order_id'] = $data['invoice_no'];
			$payment['merchant_id'] = "014011000025011";
			$payment['secret_key'] = "13E079A6330042C76165420249C920EC6FDA6B4B878A4C38BEBC354E6C68D386";
			$payment['payment_url'] = "https://t.2c2p.com/RedirectV3/payment";
		} elseif ($data['payment_channel'] == 4) {
			$payment['order_id'] = $data['invoice_no'];
			$payment['merchant_id'] = "014021000025011";
			$payment['secret_key'] = "1615F4C45C69AC894294E3AC826E286405BD2BD234649BCC9565141EA08EA0B3";
			$payment['payment_url'] = "https://t.2c2p.com/RedirectV3/payment";
		} elseif ($data['payment_channel'] == 1) {
			$payment['order_id'] = time();
			$payment['merchant_id'] = "014011000025011";
			$payment['secret_key'] = "B96DE042E4E69075CC3D52A960898E0F1560E1069D9E699C945783FC3A590238";
			$payment['payment_url'] = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
		}

		// $payment['invoice_id'] = $invoice_id;
		$payment['version'] = "8.5";
		// $payment['merchant_id'] = "014021000025011";
		$payment['currency'] = "764";
		// $payment['result_url_1'] = "http://sale.it.ditc.cloud/2c2p/result.php";
		$payment['result_url_1'] = base_url('payment/success?invoice_id=') . $invoice_id;
		// $payment['secret_key'] = "1615F4C45C69AC894294E3AC826E286405BD2BD234649BCC9565141EA08EA0B3";
		#$payment['secret_key'] = "B96DE042E4E69075CC3D52A960898E0F1560E1069D9E699C945783FC3A590238";
		$payment['payment_description'] = "Prime " . $data['invoice_no'];
		$payment['price_explode'] = explode('.', $data['total']);
		$payment['price'] = str_pad($payment['price_explode']['0'], 10, '0', STR_PAD_LEFT);
		$payment['price_dot'] = str_pad($payment['price_explode']['1'], 2, '0', STR_PAD_RIGHT);
		$payment['amount'] = $payment['price'] . $payment['price_dot'];
		$payment['params'] = $payment['version'] . $payment['merchant_id'] . $payment['payment_description'] . $payment['order_id'] . $payment['currency'] . $payment['amount'] . $payment['result_url_1'];
		$payment['hash_value'] = hash_hmac('sha256', $payment['params'], $payment['secret_key'], false);

		return $payment;
	}

	public function getReportPayment($start_date = null, $end_date = null)
	{
		$this->db->where("state", "1");
		$this->db->where(array('created_date >=' => $start_date .' 00:00:01 ','created_date <=' => $end_date .' 23:59:59'));
		$query = $this->db->get('invoice');

		$invoice = $query->result();
		
		foreach ($invoice as $key => $value) {
			$value->chanel = $this->getFeeByID($value->payment_channel);
		}

		return $query->result();
	}

	public function getReportPaymentComplete($start_date = null, $end_date = null)
	{
		$this->db->where("state", "1");
		$this->db->where("payment_status", "2");
		$this->db->where(array('payment_date >=' => $start_date .' 00:00:01 ','payment_date <=' => $end_date .' 23:59:59'));
		$query = $this->db->get('invoice');

		$invoice = $query->result();
		
		foreach ($invoice as $key => $value) {
			$value->chanel = $this->getFeeByID($value->payment_channel);
		}

		return $query->result();
	}

	public function getReportPaymentUnComplete($start_date = null, $end_date = null)
	{
		$this->db->where("state", "1");
		$this->db->where("payment_status", "1");
		$this->db->where(array('created_date >=' => $start_date .' 00:00:01 ','created_date <=' => $end_date .' 23:59:59'));
		$query = $this->db->get('invoice');

		$invoice = $query->result();
		
		foreach ($invoice as $key => $value) {
			$value->chanel = $this->getFeeByID($value->payment_channel);
		}

		return $query->result();
	}
}
