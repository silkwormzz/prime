<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getMember($username, $password)
	{
		$this->db->select('id, username, password, permission');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('state', '1');
		$this->db->limit('1');
		$query = $this->db->get('user');
		// var_dump($query->num_rows());
		// exit();
		
		if($query->num_rows() == 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
}